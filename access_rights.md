# Tools access rights

We follow the rule of the least access rights (https://en.wikipedia.org/wiki/Principle_of_least_privilege)

If you feel you need more access right, ask for them it won't be a problem, but it won't be present by default.

